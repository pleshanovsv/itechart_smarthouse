package com.itechart.smarthouse.service;

import com.itechart.smarthouse.common.UserRole;
import com.itechart.smarthouse.dao.model.Role;
import com.itechart.smarthouse.dao.model.User;
import com.itechart.smarthouse.dao.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

@Service
public class UserService {

    @Autowired
    RoleService roleService;
    @Autowired
    UserRepository userRepository;

    @Transactional(readOnly = true)
    public User getByUsername(String username) {
        User user = userRepository.findByUsername(username);
        return user;
    }

    @Transactional
    public User initNewUser(String username, String password) {
        User user = new User(username, password);
        Role userRole = roleService.getByName(UserRole.USER);
        user.setRoles(Collections.singleton(userRole));
        userRepository.save(user);
        return user;
    }
}
