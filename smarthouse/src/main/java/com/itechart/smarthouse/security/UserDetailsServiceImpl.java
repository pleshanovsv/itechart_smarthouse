package com.itechart.smarthouse.security;

import com.itechart.smarthouse.common.UserRole;
import com.itechart.smarthouse.dao.model.Role;
import com.itechart.smarthouse.dao.model.User;
import com.itechart.smarthouse.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.getByUsername(username);
        return new org.springframework.security.core.userdetails.User(username,
                user.getPassword(),
                true,
                true,
                true,
                true,
                initAuthorities(user));
    }

    private Collection<? extends GrantedAuthority> initAuthorities(User user) {
        Set<Role> userRoleList = user.getRoles();
        List<GrantedAuthority> authorities = userRoleList.stream().map(role -> {
            UserRole userRole = role.getName();
            String userRoleName = userRole.name();
            SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(userRoleName);
            return simpleGrantedAuthority;
        }).collect(Collectors.toList());
        return authorities;
    }
}
