package com.itechart.smarthouse.dao.model;

import com.itechart.smarthouse.common.UserRole;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "role")
@Getter
@Setter
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "name")
    @Enumerated(EnumType.STRING)
    private UserRole name;

}
