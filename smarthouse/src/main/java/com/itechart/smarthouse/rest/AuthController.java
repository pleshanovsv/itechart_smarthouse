package com.itechart.smarthouse.rest;

import com.itechart.smarthouse.dao.model.User;
import com.itechart.smarthouse.rest.request.LoginRequest;
import com.itechart.smarthouse.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    AuthService authService;

//    @PostMapping("/signin")
    @RequestMapping(value = "/signin", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        try {
            String username = loginRequest.getUsername();
            String password = loginRequest.getPassword();
            authService.isAuthenticate(username, password);
            return RestUtils.buildResponseOk();
        } catch (Exception e) {
            return RestUtils.buildResponseError(e);
        }
    }

    @PostMapping("/signup")
    public ResponseEntity registerUser(@Valid @RequestBody LoginRequest loginRequest) {
        try {
            String username = loginRequest.getUsername();
            String password = loginRequest.getPassword();
            User user = authService.registerUser(username, password);
            return RestUtils.buildResponseCreated(user.getId().toString());
        } catch (Exception e) {
            return RestUtils.buildResponseError(e);
        }
    }
}
