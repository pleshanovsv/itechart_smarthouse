package com.itechart.smarthouse.rest;

import com.itechart.smarthouse.dao.model.User;
import com.itechart.smarthouse.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/{username}")
    public User getUser(@PathVariable String username) {
        User user = userService.getByUsername(username);
        return user;
    }

}
