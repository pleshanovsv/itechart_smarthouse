package com.itechart.smarthouse.common;

public enum UserRole {
    ADMIN,
    USER,
    GUEST
}
