package com.itechart.smarthouse.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.Objects;

public class RestUtils {

    public static <T> ResponseEntity buildResponseGet(T object) {
        if (Objects.isNull(object)) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } else if (object instanceof Collection && CollectionUtils.isEmpty((Collection<?>) object)) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } else {
            return ResponseEntity.ok(object);
        }
    }

    public static ResponseEntity buildResponseCreated(String str) {
        return ResponseEntity.status(HttpStatus.CREATED).body(str);
    }

    public static ResponseEntity buildResponseError(Exception e) {
        String message = e.getMessage();
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(message);
    }

    public static ResponseEntity buildResponseOk() {
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}
