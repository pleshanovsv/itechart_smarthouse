package com.itechart.smarthouse.service;

import com.itechart.smarthouse.common.UserRole;
import com.itechart.smarthouse.dao.model.Role;
import com.itechart.smarthouse.dao.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoleService {

    @Autowired
    RoleRepository roleRepository;

    @Transactional(readOnly = true)
    public Role getByName(UserRole userRole) {
        Role role = roleRepository.findByName(userRole);
        return role;
    }

}
