package com.itechart.smarthouse.dao.repository;

import com.itechart.smarthouse.common.UserRole;
import com.itechart.smarthouse.dao.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByName(UserRole roleName);
}
